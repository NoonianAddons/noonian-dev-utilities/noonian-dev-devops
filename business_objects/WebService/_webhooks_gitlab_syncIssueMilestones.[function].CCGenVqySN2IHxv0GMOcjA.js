function (db, postBody, Q, _, req, GitlabUtil) {
    
    console.log('***GITLAB ISSUE WEBHOOK***');
    const issId = postBody && postBody.object_attributes && postBody.object_attributes.iid;
    console.log(issId);
    console.log('**************************');
    
    const passedToken = req.get('X-Gitlab-Token');
    
    
    return db.GitlabProject.findOne({webhook_token:passedToken}).then(gp=>{
        if(!gp) {
            throw 'Invalid Request';
        }
        
        const enabled = _.get(gp, 'config.enableMilestoneLabels');
        if(!enabled) {
            console.error(`*** enableMilestoneLabels not enabled for GitlabProject ${gp.key} ***`);
            return {result:'disabled'};
        }
    
    
        const labelDelta = _.get(postBody, 'changes.labels');
        
        if(labelDelta) {
            
            //scan affected labels to see if any Milestone-related lables have changed.
            
            const msRegex = /^MS_/;
            
            let pre = _.pluck(labelDelta.previous, 'title');
            let post = _.pluck(labelDelta.current, 'title');
            
            let affected = _.difference(pre, post).concat(_.difference(post, pre));
            
            let msRelated = false;
            affected.forEach(l=>{
                msRelated = msRelated || msRegex.test(l)
            });
            
            if(msRelated) {
                console.log('MILESTONE CHANGE!!!!!');
                var promKey = `IN_PROGRESS_${gp._id}_${issId}`;
                GitlabUtil[promKey] = GitlabUtil.syncIssueMsLabels(gp._id, issId).then(r=>{
                    console.log(r);
                    GitlabUtil[promKey] = null;
                });
            }
            
        }
        
        
        return {result:'success'}; 
    });
    
}