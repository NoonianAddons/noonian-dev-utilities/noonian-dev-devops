function(logger) {
    logger = logger.get('devops.gitlab');
    
    return function(ms, issueStatus) {
        logger.debug('getMilestoneIssues:', ms, issueStatus);
        
        if(!this.project_id) {
            throw new Error('Missing project id for project '+this.key);
        }
        
        issueStatus = issueStatus || 'opened';
        
        const api = this.getApi();
        return api.Issues.all({projectId:this.project_id, milestone:ms.title, state:issueStatus});
    };
    
    
    
}