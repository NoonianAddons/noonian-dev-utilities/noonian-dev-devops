function(logger) {
    logger = logger.get('devops.gitlab');
    
    return function(id) {
        logger.debug('deleteLabel', id);
        
        if(!this.project_id) {
            throw new Error('Missing project id for project '+this.key);
        }
        
        if(!id) {
            throw new Error('label id required');
        }
        
        const api = this.getApi();
        return api.Labels.remove(this.project_id, id);
    };
    
}