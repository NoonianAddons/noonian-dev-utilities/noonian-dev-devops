function(logger) {
    logger = logger.get('devops.gitlab');
    
    return function() {
        logger.debug('getLabels');
        
        if(!this.project_id) {
            throw new Error('Missing project id for project '+this.key);
        }
        
        const api = this.getApi();
        return api.Labels.all(this.project_id);
    }
}