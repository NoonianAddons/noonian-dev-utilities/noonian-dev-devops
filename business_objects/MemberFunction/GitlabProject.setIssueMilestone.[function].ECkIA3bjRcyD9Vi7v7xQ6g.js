function(logger, GitlabUtil) {
    logger = logger.get('devops.gitlab');
    
    
    return function(iss, ms) {
        logger.debug('setIssueMilestone: %j', iss, ms);
        
        if(!this.project_id) {
            throw new Error('Missing project id for project '+this.key);
        }
        
        var milestone_id = ms && ms.id || 0; //Zero unsets milestone
        
        var labels;
        if(this.config && this.config.enableMilestoneLabels) {
            labels = GitlabUtil.syncIssueMsLabelList(iss.labels || [], ms).join(',');
        }
        
        const api = this.getApi();
        return api.Issues.edit(this.project_id, iss.iid, {milestone_id, labels});
    };
    
}