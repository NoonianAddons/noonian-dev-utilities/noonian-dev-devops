function(logger, GitlabUtil) {
    logger = logger.get('devops.gitlab');
    
    
    return function(iss, message) {
        logger.debug('closeIssue:', iss, message);
        
        if(!this.project_id) {
            throw new Error('Missing project id for project '+this.key);
        }
        
        //remove Dev Done Label if present
        let pos = iss.labels.indexOf(GitlabUtil.DEV_DONE_TAG);
        if(pos > -1) {
            iss.labels.splice(pos, 1);
        }
        
        const api = this.getApi();
        return api.Issues.edit(this.project_id, iss.iid, {state_event:'close', labels:iss.labels.join(',')}).then(closeRes=>{
            return api.IssueNotes.create(this.project_id, iss.iid, message).then(res=>[closeRes, res]);
        });
        
    };
    
}