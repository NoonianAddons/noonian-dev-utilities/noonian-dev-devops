function(logger, GitlabUtil, moment, Q) {
    logger = logger.get('devops.gitlab');
    
    
    return function(message) {
        logger.debug('closeNextActiveMilestone:', message);
        
        return this.getNextActiveMilestone().then(nextMs=>{
            let now = moment();
            if(now.isAfter(nextMs.due_date)) {
                
                var firstProm;
                if(this.config && this.config.enableMilestoneLabels) {
                    let myLabel = GitlabUtil.getMilestoneLabel(nextMs);
                    firstProm = this.deleteLabel(myLabel);
                }
                else {
                    firstProm = Q(true);
                }
                
                let myLabel = MS_LABEL_PREFIX+nextMs.due_date;
                
                return firstProm.then(()=>{
                    const api = this.getApi();
                    return api.ProjectMilestones.edit(this.project_id, nextMs.id, {
                        state_event:'close', 
                        description:message
                    });
                });
            }
            else {
                return nextMs;
            }
        });
        
        
    };
    
}