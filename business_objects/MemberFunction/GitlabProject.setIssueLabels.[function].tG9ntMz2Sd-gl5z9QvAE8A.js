function(logger) {
    logger = logger.get('devops.gitlab');
    
    return function(iss, labels) {
        logger.debug('set labels:', iss, labels);
        
        if(!this.project_id) {
            throw new Error('Missing project id for project '+this.key);
        }
        
        if(!iss) {
            throw new Error('issue required');
        }
        
        if(labels instanceof Array) {
            labels = labels.join(',');
        }
        
        const api = this.getApi();
        return api.Issues.edit(this.project_id, iss.iid, {labels});
    };
    
}