function(logger) {
    logger = logger.get('devops.gitlab');
    
    return function(ms) {
        logger.debug('createMilestone:', ms);
        if(!this.project_id) {
            throw new Error('Missing project id for project '+this.key);
        }
        
        
        const api = this.getApi();
        return  api.ProjectMilestones.create(this.project_id, ms.title, ms);
    };
    
    
}